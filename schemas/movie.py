from pydantic import BaseModel, Field, List, Dict 
from typing import Optional

#se importa la clase movie de basemodel, junto con el schema
class Movie(BaseModel):
    id: Optional[int] = None #Indicamos que es opcional
    title: str = Field(min_length=5)
    overview: str = Field(min_length=15)
    year: int = Field(le=2024)
    rating: float = Field(ge=1, le=10)
    category: str = Field(min_length=5, max_length=15)

#me falta el schema

class Config:
    json_schema_extra = {
        "example": {
            "titulo":"Titulo del libro",
            "autor":"Autor del libro",
            "año":"2020",
            "categoria":"Categoria",
            "codigo": 0,
            "numeroDePaginas": "100"
        }
    }


