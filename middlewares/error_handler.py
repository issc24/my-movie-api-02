from starlette.middleware.base import BaseHTTPMiddleware
from fastapi import FastAPI, Request, Response
from fastapi.responses import JSONResponse




class ErrorHandler(BaseHTTPMiddleware):
    #constructor para mandarlo a llamar al main
    def __init__(self, app: FastAPI) -> None:
        super().__init__(app)
    # solicitud, respuesta y llamada al siguiente
    async def dispatch(self, request: Request, call_next) -> Response:
        try:
            return await call_next(request) 
        except Exception as e:
            return JSONResponse(status_code=500, content={'error': str(e)})


