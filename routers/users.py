from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends#Se utiliza el Body para el @app.POST que nos ayuda a agilizar el trabajo.
from fastapi.responses import HTMLResponse, JSONResponse # Para trabajar con el protocolo https, el JSON nos ayuda para 
# CREAR UN ESQUEMA POR DEFAULT, (PLANTILLA) MANDARLO A LLAMAR EN EL MÉTODO PARA ASÍ MANDARLO A LLAMAR
from pydantic import BaseModel  # el modelo base va creando el esquema
from typing import List
#Imort para el TOKEN
from utils.jwt_manager import create_token, validate_token
from fastapi import APIRouter
#Import para la Base de Datos
from config.database import Session, engine, Base
from models.movie import Movie as MovieModel # se le pone otro nombre para que no truene por la clase que esté en el Main
from fastapi.encoders import jsonable_encoder
#Routers
from routers.movie import movie_router
from schemas.user import User

user_router = APIRouter()


@user_router.post('/login', tags=['auth'])
def login(user: User):
    if user.email == "admin@gmail.com" and user.password == "admin":
        token: str = create_token(user.dict())
        return JSONResponse(status_code=200, content=token)

