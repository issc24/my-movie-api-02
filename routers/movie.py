from fastapi import Path, Query, Depends,  APIRouter#Se utiliza el Body para el @app.POST que nos ayuda a agilizar el trabajo.
from fastapi.responses import JSONResponse # Para trabajar con el protocolo https, el JSON nos ayuda para 
# CREAR UN ESQUEMA POR DEFAULT, (PLANTILLA) MANDARLO A LLAMAR EN EL MÉTODO PARA ASÍ MANDARLO A LLAMAR

from pydantic import BaseModel, Field # el modelo base va creando el esquema
from typing import Optional, List

#Import para la Base de Datos
from config.database import Session
from models.movie import Movie as MovieModel # se le pone otro nombre para que no truene por la clase que esté en el Main
from fastapi.encoders import jsonable_encoder

#Import para el Middleware

from middlewares.jwt_bearer import JWTBearer

#importamos el servicio
from services.movie import MovieService

#importacion de schemas
from schemas.movie import Movie

# creamos la instancia de movie_router
movie_router = APIRouter()



#Obtener movies 
@movie_router.get('/movies', tags=['movies'], response_model= List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies() ####3 se agregaron estos
    return JSONResponse(status_code=200, content= jsonable_encoder(result))

#Obtener movies por ID
@movie_router.get('/movies/{id}', tags=['movies'])
def get_movie(id: int = Path(ge=1, le=2000)) -> Movie:
    db = Session()
    result = MovieService(db).get_movie(id) ####3 se agregaron estos
    if not result:
        return JSONResponse(status_code=404, content={'message:' 'No encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

#Obtener movies por categoria o algo asi
@movie_router.get('/movies/', tags=['movies'])
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)) -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movieCat(category) ####3 se agregaron estos
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


#Crear una nueva película
@movie_router.post('/movies', tags=['movies'])
def create_movie(movie: Movie) -> dict:
    db = Session()
    MovieService(db).create_movie(movie) ####3 se agregaron estos
    return JSONResponse(content={"message": "Se ha registrado la pelicula"})

# Actualizar una nueva Peícula
@movie_router.put('/movies/{id}', tags=['movies'], response_model= dict, status_code=200)
def update_movie(id: int, movie: Movie) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id) # cambio de esta líena
    if not result:
        return JSONResponse(status_code=404, content={'message: ' 'No encontrado'})
    
    MovieService(db).update_movie(id, movie) # cambio de esta linea para 3er parcial.
    return JSONResponse(status_code=200, content={'message: ' 'Se ha modificado la película :D'})

#Borrar una Película
@movie_router.delete('/movies/{id}', tags=['movies'], response_model= dict, status_code=200)
def delete_movie(id: int) -> dict:
    db = Session()
    result = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:    
        return JSONResponse(status_code=404, content={"message": "No encontrada"})
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200, content='Message: ' "Se eliminó la Película")

