# configuración para la base de datos

import os;
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

sqlite_file_name = "../database.sqlite" # en donde quiero que se me configure la bse de datos, así con el nombre

# Estamos leyendo el directorio actual que es database.py
base_dir = os.path.dirname(os.path.realpath(__file__))

# Creamos la url de la base de datos uniendo las 2 variables anteriores
databes_url = f"sqlite:///{os.path.join(base_dir, sqlite_file_name)}"

engine = create_engine(databes_url, echo=True)

#crear una sesion para acceder a la base de datos
Session = sessionmaker(bind=engine)

Base = declarative_base()