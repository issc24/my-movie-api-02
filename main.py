""" from fastapi import FastAPI, Body, Path, Query, Request, HTTPException, Depends#Se utiliza el Body para el @app.POST que nos ayuda a agilizar el trabajo.
from fastapi.responses import HTMLResponse, JSONResponse # Para trabajar con el protocolo https, el JSON nos ayuda para 
# CREAR UN ESQUEMA POR DEFAULT, (PLANTILLA) MANDARLO A LLAMAR EN EL MÉTODO PARA ASÍ MANDARLO A LLAMAR
from fastapi.security.http import HTTPAuthorizationCredentials
from pydantic import BaseModel, Field # el modelo base va creando el esquema
from typing import Coroutine, Optional, List

#Imort para el TOKEN
from jwt_manager import create_token, validate_token
from fastapi.security import HTTPBearer

#Import para la Base de Datos
from config.database import Session, engine, Base
from models.movie import Movie as MovieModel # se le pone otro nombre para que no truene por la clase que esté en el Main
from fastapi.encoders import jsonable_encoder

#Import para el Middleware
from middlewares.error_handler import ErrorHandler
from middlewares.jwt_bearer import JWTBearer

#Routers
from routers.movie import movie_router
 """
from fastapi import FastAPI
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel
from utils.jwt_manager import create_token
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router

app = FastAPI()
app.title = "Mi primera chamba con APIS"

#Middleware, que encuentre el error
app.add_middleware(ErrorHandler)
app.include_router(movie_router)

#Nos traemos la BD
Base.metadata.create_all(bind=engine)


@app.get('/', tags=['home'])
def message():
    return HTMLResponse('<h1>Esto es FastAPI con Movies & Compu</h1>')




"""""
####### ACTIVIDAD ######
####### ACTIVIDAD ######

#Imort para el TOKEN
from jwt_manager import create_token, validate_token
from fastapi.security import HTTPBearer


#Import para la Base de Datos
from config.database import Session, engine, Base
from models.computadora import Computadora as CompuModel # se le pone otro nombre para que no truene por la clase que esté en el Main
from fastapi.encoders import jsonable_encoder


# hacer una clase/instancia para el EndPonit
class User(BaseModel):
    email:str
    password:str


class JWTBearer(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data['email'] != "admin@gmail.com":
            raise HTTPException(status_code=403, detail="Credenciales Incorrectas")


#Nos traemos la BD
Base.metadata.create_all(bind=engine)

class Computadoras(BaseModel):
    id: Optional[int] = None #Indicamos que es opcional
    marca: str = Field(min_length=2, max_length=20)
    modelo: str = Field(min_length=2, max_length=50)
    color: str = Field(min_length=2, max_length=50)
    ram: str = Field(min_length=2, max_length=20)
    almacenamiento: str = Field(min_length=2, max_length=20)

    class Config:
        json_schema_extra ={
            "example":{
                "id": 1,
                "marca": "Asus",
                "modelo": "Ferrari 704",
                "color": "Rojo",
                "ram": "16 Gb",
                "almacenamiento": "256 GB SSD"
            }
        }
        
computadoras = [
    {
        "id": 1,
        "marca": "Apple",
        "modelo": "Mackbook M3 Pro",
        "color":"Space Gray",
        "ram": "42 Gb",
        "almacenamiento":"1 TB SSD"
    },
    {
       "id": 2,
        "marca": "Lenovo",
        "modelo": "Thinkpad X1",
        "color":"Negro",
        "ram": "16 Gb",
        "almacenamiento":"500 GB SSD"
    },
    {
        "id": 3,
        "marca": "Asus",
        "modelo": "Vivobook",
        "color":"Gris",
        "ram": "64 Gb",
        "almacenamiento":"512 GB SSD"
    },
    {
          "id": 4,
        "marca": "Apple",
        "modelo": "Macbook Air M1",
        "color":"Space Gray",
        "ram": "8 Gb",
        "almacenamiento":"512 GB SSD"
    },
    {
         "id": 5,
        "marca": "HP",
        "modelo": "Probook",
        "color":"Gris",
        "ram": "64 Gb",
        "almacenamiento":"512 GB SSD"
    }
]

#Nos traemos la BD
Base.metadata.create_all(bind=engine)


#EndPoint que permita el usuario logearse con el TOKEN
@app.post('/login', tags=['auth'])
def login(user: User):
    if user.email == "admin@gmail.com" and user.password == "admin":
        token: str = create_token(user.dict())
        return JSONResponse(status_code=200, content=token)

# El @get para obtener las computadoras
@app.get('/computadoras', tags=['computadoras'], response_model=List[Computadoras], dependencies=[Depends(JWTBearer())])
def get_computadoras() -> List[Computadoras]:
    db = Session()
    result2 = db.query(CompuModel).all()
    return JSONResponse(status_code=200, content= jsonable_encoder(result2))

# @get para obtener las computadoras por su ID
@app.get('/computadoras/{id}', tags=['computadoras'], dependencies=[Depends(JWTBearer())])
def get_computadora_by_id(id: int = Path(ge=1, le=2000)) -> Computadoras:
    db = Session()
    resultt = db.query(CompuModel).filter(CompuModel.id == id).first()
    if not resultt:
        return JSONResponse(status_code=404, content={"message": "No encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(resultt))


# @get para obtener las computadoras por la marca   
@app.get('/computadoras/', tags=['computadoras'], dependencies=[Depends(JWTBearer())])
def get_computadoras_by_marca(marca: str = Query(min_length=5, max_length=15)) -> List[Computadoras]:
    db = Session()
    resultt = db.query(CompuModel).filter(CompuModel.marca == marca).all()
    return JSONResponse(status_code=200, content=jsonable_encoder(resultt))


### ACTIVIDAD ###
# Generar un CRUD para computadoras

# @post lo utilizamos para agregar una nueva computadora y que además lo muestre en la DB
@app.post('/computadoras/', tags=['computadoras'], response_model= List[Computadoras], status_code=200, dependencies=[Depends(JWTBearer())])
def post_computadora(computadora: Computadoras) -> dict:
    db = Session()
    new_computadora = CompuModel(**computadora.model_dump())
    db.add(new_computadora)
    db.commit()
    computadoras.append(computadora)
    return JSONResponse(status_code=200, content={"message": "Se ha registrado la computadora"})

# @put para actualizar
@app.put('/computadoras/{id}', tags=['computadoras'], response_model= List[Computadoras], status_code=200, dependencies=[Depends(JWTBearer())])
def update_computadora(id: int, computadora: Computadoras) -> dict:
    db = Session()
    resultt = db.query(CompuModel).filter(CompuModel.id == id).first()
    if not resultt:
        return JSONResponse(status_code=404, content={"message": "No encontrado"})
    resultt.marca = computadora.marca
    resultt.modelo = computadora.modelo
    resultt.color = computadora.color
    resultt.ram = computadora.ram
    resultt.almacenamiento = computadora.almacenamiento
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha modificado la computadora"})


# @delete para eliminar una compu
@app.delete('/computadoras/{id}', tags=['computadoras'], response_model= List[Computadoras], status_code=200, dependencies=[Depends(JWTBearer())])
def delete_computadora(id: int) -> dict:
    db = Session()
    resultt = db.query(CompuModel).filter(CompuModel.id == id).first()
    if not resultt:
        return JSONResponse(status_code=404, content={"message": "No encontrado"})
    db.delete(resultt)
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha eliminado la computadora"})
"""